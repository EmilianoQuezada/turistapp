let marcadorDestino = null; //Variable global para poder resetar los marcadores de las ubicaciones
let coordenadasDestino= null; //Variable global para poder almacenar las coordenadas de destino para utilizar como parametro en la funcion de ruteo
let coordenadasInicio= null; //Variable global para poder almacenar las coordenadas de inicio para utilizar como parametro en la funcion de ruteo
let controlRuta = null; //Variable global para controlar el estado del ruteo y poder detenerlo
let info = null; //Variable global para almacenar la información general del destino
let img = null; //Variable global para almacenar la src de la imagen

//Instanciamos los botones
ComoLlegar = document.getElementById('btnLLegar');
Cancelar = document.getElementById('btnCancelar');

//Instanciamos el select
ComboBox = document.getElementById('Destinos');

// Canva del mapa utilizando leaflet y ligandolo a nuestro div

//                  id             lat         long     zoom                                    
let map = L.map('mapa').setView([23.26544, -106.373972], 18);

// Agregar el marco con creditos el mapa
L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

//Funcion para extraer la ubicacion actual del dispositivo

// Verifica si el navegador soporta geolocalización con uso de API integrada en el navegador
if ('geolocation' in navigator) {
    navigator.geolocation.getCurrentPosition(function(position) {
        //Alerta para notificar el uso seguro de la ubicación del usuario
        alert("La ubicacion es para mejorar la eficiencia del sistema, no se almacena en ningun lado");
        // Obtiene la posición actual
        var MiUbicacion = [position.coords.latitude, position.coords.longitude];
        //Almacenamos las coordenadas del usuario en la variable global para utilizarlas en funciones
        coordenadasInicio = MiUbicacion;
        // Actualiza la vista del mapa a la ubicación actual
        map.setView(MiUbicacion, 15);
        // Añade un marcador en la ubicación actual
        L.marker(MiUbicacion).addTo(map).bindPopup("Mi ubicación Actual");
        // Mensaje en caso de error
    }, function(error) {
        console.log(error);
        alert("Geolocalización falló o no fue permitida.");
    });
} else {
    //Mensaje en caso de que el navegador no coincida con la funcion
    alert("Geolocalización no es soportada por este navegador.");
}



//Funcion para mostrar destinos

//                     id del select   cada que se seleccione un elemento se actualiza la función
ComboBox.addEventListener('change', function(e){
    //Convierte las coordenadas del value de cada elemento 
    let coordenadas = e.target.value.split(",");
    //Utiliza la funcion de Leaaflet FlyTo para mostrar donde esta el destino
    map.flyTo(coordenadas,18);
    //Obtenemos el nombre de la ubicación en la variable
    let destino = e.target.options[e.target.selectedIndex].text;
    console.log(destino);
    //Validación de marcador adicional al de la ubicación del usuario
    if(marcadorDestino){
        //Acción para remover el marcador adicional si es que ya existia una
        marcadorDestino.remove();
    }
    //Creacion del marcador de destino
    marcadorDestino = L.marker(coordenadas).addTo(map).bindPopup(destino).openPopup();

    //Mandamos a llamar la función para cargar la foto e información del lugar en variables
    Cargarinfo(destino);

    //Creamos un nuevo div y lo agregamos al contenedor
    let infoContenedor = document.getElementById('Info-Contenedor').innerHTML = 
    '<h1>'+destino+'</h1>'+
    '<img src="'+img+'" alt="">'+
    '<p>'+info+'</p>'; 

    //Activamos el boton con id "btnLLegar"
    ComoLlegar.style.display = 'block';
    //Almacenamos las coordenadas de destino en la variable global para utilizarlas en funciones
    coordenadasDestino = coordenadas;
});


function Cargarinfo(destino){
    if(destino == "Acuario Mazatlán"){
        info =  "El acuario tiene un horario de 7am a 4pm aunque el precio es de 380 pesos por persona, lo vale ya que estas ayudando a cuidar las especies y a mantenerlas <br> Es una buena experiencia y más si te gusta o tienes curiosidad acerca de la vida marina. <br> Se recomienda acudir en familia para disfrutar aun mas esta gran locación en compañia de tus seres amados"
        img= "IMG/Acuario.jpg"
    }
    else if(destino == "Catedral - Basílica de la Inmaculada Concepción"){
        info =  "La catedral esta abierta a todo el publico de 7am a 7pm se puede asistir a cualquira de estas horas y esuchar misa <br> Incluso si tu no compartes la misma pasión religiosa deberias considerar visitar la Catedral dado a su hermosa arquitectura gótica lo cual la vuelve ideal para aquellos amantes de la arquitectura y cultura"
        img= "IMG/catedral.jpg"
    }
    else if(destino == "El clavadista"){
        info =  "Para ver a los clavadistas se recomienda ir viernes, sábado, domingo entre 6 a 10 pm que es cuando se puede disfrutar el espectáculo brindado por los temerarios clavadistas. <br> Además del espectaculo puedes gozar de los puestos que hay en su alrededor para disfrutar de un antojo mientras disfrutas de este gran punto de interes "
        img= "IMG/Clavadista.jpg"
    }
    else if(destino == "El mirador"){
        info =  "El mirador se recomienda en los amaneceres y atardeceres para tener mejor experiencia dado a las vistas que ofrece jugando con el horizonte del mar en conjunto con el sol, también lo hace un hermoso sitio para disfrutar en pareja y tomar fotos para guardar la maravillosa experiencia"
        img= "IMG/mirador.jpg"
    }
    else if(destino == "Letras Mazatlán"){
        info =  "Sin duda alguna este punto de interés es una atracción simple pero interesante debido a sus hermosas vistas por parte del malecón, además de contar con diversos lugares interesantes. <br> Un sitio ideal para tomar una foto en  tus redes sociales y para tomarlo como punto de partida para seguir disfrutando del puerto."
        img= "IMG/letras.jpg"
    }
    else if(destino == "Plazuela Machado"){
        info =  "La plazuela machado ofrece diferentes actividades dependiendo la temporada pero siempre podrás encontrar buenas vistas, restaurantes, musica en vivo, etc. Además de contar con una arquitectura clasica en la gran mayoría de los edificios de  sus alrededores lo convierten en una excelente atracción para aquellos amantes de la historia y cultura. <br> Siempre es buena hora para visitar la plazuela pero sin duda la experiencia mejora en la noche con la bella iluminación que cuenta además es en esta hora donde se puede sentir más el ambiente tanto de los habitantes locales como de otros turistas que visitan el puerto para una experiencia grata."
        img= "IMG/Machado.webp"
    }
    else{

    }
    
}


//Función para dibujar la ruta óptima entre la ubicación del usuario y del destino
//         Ubicación usuario   Ubicación destino
function Trazado(inicio,           destino) {
    //Validación de trazado de rutas
    if (controlRuta != null) {
        map.removeControl(controlRuta); //Acción para remover el trazado adicional si es que ya existia una
    }
    //Creación del trazado de rutas almacenado en una variable para el control 
    controlRuta = L.Routing.control({
        waypoints: [
            L.latLng(inicio), //Ubicación usuario
            L.latLng(destino) //Ubicación destino
        ]
    }).addTo(map); //Añade el trazado de rutas al mapa
}
//Funcion para detener el trazado de rutas
function DetenerTrazado() {
    //Validación de trazado de rutas
    if (controlRuta != null) {
        map.removeControl(controlRuta); //Acción para remover el trazado adicional si es que ya existia una
        controlRuta = null; //Reseteamos el valor de la variable destinada al control
    }
}

//Escuchador del boton Cómo llegar
document.getElementById('btnLLegar').addEventListener('click', function() {
    Trazado(coordenadasInicio, coordenadasDestino); //Mandamos a llamar la funcion de trazado de rutas con la ubicacion del usuario y destino como parámetros
    //Ocultamos el boton Cómo llegar para prevenir mandar a llamar la función que esta en ejecución o sobreescrita con otros parametros
    ComoLlegar.style.display = 'none'; 
    //Mostramos el boton Cancelar para darle la opción al usuario de detener la función
    Cancelar.style.display = 'block';
    //Desabilitamos el select para evitar problemas con la función del sistema
    ComboBox.disabled= true;
    //Mostramos el mapa en la ubicación del usuario
    map.flyTo(coordenadasInicio,18);

});

//Escuchador del boton Cancelar
document.getElementById('btnCancelar').addEventListener('click', function() {
    //Mandamos a llamar la función para detener el trazado de rutas
    DetenerTrazado();
    //Ocultamos el boton Cancelar para no mandar a llamar una función DetenerTrazado
    ComoLlegar.style.display = 'block';
    //Mostramos el botón Cómo llegar para reactivar la función de trazado de rutas sin necesidad de tener que cambiar el evento del select
    Cancelar.style.display = 'none';
    //Desabilitamos el select para que el usuario pueda seleccionar un nuevo destino
    ComboBox.disabled= false;
});